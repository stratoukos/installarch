#!/bin/sh

# Arch installation script with these reasonable (for me) assumptions:
#
# -
# -	single disk called /dev/sda
# -	network available during the installation using dhcp
#
# It will do the following
#
# -	guid partition table and single root partition spannng the entire disk
# -	install just base system + gptfdisk + syslinux + ntpd
# -	configure syslinux to autoboot
# -	set Greek pacman mirrorlst
# -	set locale to en_US.UTF-8
# -	set timezone to Europe/Athens
# -	configure some minor stuff according to my preferences
#
# This script is supposed to setup everything that is identical between all
# my arch installations, so you will need to configure some stuff by hand
# after the installation. Check the bottom of this script for a checklist
#
# The script is supposed to be used for documentation. You should type all
# commands yourself and verify ther effects afterwards. That said, everything
# here is non-interactive and the script handles chrooting correctly, so you
# could `. =(curl http://mioumiou.eu/install-arch.sh)` when you boot from the
# live cd and end up with a correctly installed system. Please don't do this
# unless you are me or you happen to want an installation 100% identical to
# mine. Additionaly, I reserve the right to modify the script in the above url
# to download obscene amounts of donkey porn.
#
# The script has only been tested with the archlinux-2013.11.01 live cd.

# setup and mount partitions
parted -s /dev/sda mklabel gpt
# try to align your partition (http://rainbow.chard.org/2013/01/30/how-to-align-partitions-for-best-performance-using-parted/)
parted -s /dev/sda mkpart primary ext4 2048s 100%  # aligned for me
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt

# get a greek mirrorlist
curl 'https://www.archlinux.org/mirrorlist/?country=GR' | sed -e 's/#//' > /etc/pacman.d/mirrorlist

# install base system
pacstrap /mnt base

# generate fstab
genfstab -U -p /mnt >> /mnt/etc/fstab

# chroot into the installation to configure our new system
arch-chroot /mnt /bin/sh << 'EOS'

# set locale and timezone
sed -i 's/^#\(en_US.UTF-8.*\)/\1/' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
ln -s /usr/share/zoneinfo/Europe/Athens /etc/localtime

# install ntp, enable the daemon and run once to set the hardware clock
pacman --noconfirm -S ntp
systemctl enable ntpd
ntpd -q
hwclock --systohc --utc

# install and confgure the bootloader
pacman --noconfirm -S syslinux gptfdisk
syslinux-install_update -iam
sed -i 's/sda3/sda1/' /boot/syslinux/syslinux.cfg
sed -i 's/^UI /#UI /' /boot/syslinux/syslinux.cfg

# change some pacman options
sed -i 's/^#\(Color\|TotalDownload\)/\1/' /etc/pacman.conf

# exiting chroot
EOS


cat << 'EOS'
Things to do now

-	enter the chroot

	`arch-chroot /mnt`

-	set the root password

	`passwd`

-	create any additional users you want

	`useradd -m -g users -G wheel -s /bin/bash username`
	`passwd username`

-	set your hostname

	`echo my.hostna.me > /etc/hostname`

-	configure your network

	If you just need dhcp run `systemctl enable dhcpd.service` otherwise
	check https://wiki.archlinux.org/index.php/Netctl for more options

-	exit chroot, unmount disk and reboot

	`exit`
	`umount /mnt`
	`reboot`

-	have a nice cup of tea and wait for all this to blow over
EOS

